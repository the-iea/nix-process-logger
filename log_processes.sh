#!/bin/bash

# log most cpu intensive processes to file 
# to background, run as follows:
# ./log_processes.sh &

NUM_PROC=10
LOGFILE=process.log
PERIOD_SEC=2
TMPFILE=.current_processes.txt

if [ -f "$LOGFILE" ]; then
	echo "$LOGFILE exists, please delete..."
	exit
fi

# prepare logfile
echo "TIME                       %CPU PID   USER     COMMAND" >> $LOGFILE
printf "\n" >> $LOGFILE

while true
do
	TIME=`date -Iseconds`
	# throw process data into temp file for prepending by timestamp
	ps -eo pcpu,pid,user,args --no-header | sort -k 1 -nr | head -$NUM_PROC > $TMPFILE
	# prepend process lines with timestamp and append text block to main log
	cat $TMPFILE | sed 's/^/'$TIME'\ \ /g' >> $LOGFILE
	printf "\n" >> $LOGFILE
	sleep $PERIOD_SEC
done

