# Process CPU utilisation logger

A simple logging script to record which processes are occupying CPU. 

Requires basic Unix tools like sed, ps, date, printf, etc.

Tested on Ubuntu 18.04.

## How to install

Clone this repo.

## How to run

Make script executable with:

    sudo chmod u+x log_processes.sh

Run in background with:

    ./log_processes.sh &

The script does not currently perform log rotation.

## Future work

Create some simple analysis functionality.
